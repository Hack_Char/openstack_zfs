# OpenStack_ZFS

This is a custom ZFS cinder driver I use in my home lab. Since I don't intend to set up a CI/CD pipeline and do official cider driver tests whenever anyone updates Cinder,
it is not 'Official' - however, I currently use it in a home lab instance of OpenStack 2023.1.


## Getting started

The three files here must be copied to their correct directories. I use '/opendev' to clone the OpenStack PyPi code; it is likely you will need to place these
under /usr or /usr/local instead depending on how you're deploying OpenStack. 

```
cp zfs.filters /opendev/etc/cinder/rootwrap.d
cp zfs-migrate /opendev/bin
cp zfs.py /opendev/lib/python3.11/site-packages/cinder/volume/drivers
```

You may also need to compile the module 
(`py3compile /opendev/lib/python3.11/site-packages/cinder/volume/drivers/zfs.py`).

## Gotchas

This ZFS Cinder driver works for my use case. I have not tested or attempted to get every possible operation to work. If something does not work, please create an issue
or better yet make a merge request for a fix.

* Only raw images are supported on Cinder (there are some gotchas if you want to use qcow2 on Cinder - it's better to use instance storage instead and make that directory
ZFS backed to provide RAID and/or lz4 compression).

## Original code

The original driver was from an appearantly abandoned 'NFVexpress' repository on github that appears to now redirect here: https://github.com/nusov/cinder-zfs
The driver there may be a better choice - it appears to have had some updates from the original. I have not tested the updated code in that repository.


